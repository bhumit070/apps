#!/usr/bin/env bash
cd /tmp || exit
echo "Downloading and unpacking Postman... (1/5)"
wget -q https://dl.pstmn.io/download/latest/linux?arch=64 -O postman.tar.gz
tar -xzf postman.tar.gz
rm postman.tar.gz

echo "Installing to opt... (2/5)"
if [ -d "/opt/Postman" ];then
    sudo rm -rf /opt/Postman
fi
sudo mv Postman /opt/Postman

echo "Creating symbolic link... (3/5)"
if [ -L "/usr/bin/postman" ];then
    sudo rm -f /usr/bin/postman
fi
sudo ln -s /opt/Postman/Postman /usr/bin/postman

echo "Downloading .desktop file... (4/5)"
wget https://gist.githubusercontent.com/sam-sla/56f6bc4ee9e317cf8eb4efca650a4e60/raw/90d701b5eb942c07f633bd59f769333b0ced8db8/Postman.desktop

echo "Installing .desktop file... (5/5)"
if [ -e "/usr/share/applications/Postman.desktop" ];then
    sudo rm /usr/share/applications/Postman.desktop
fi
sudo mv Postman.desktop /usr/share/applications/Postman.desktop

echo "Installation completed successfully."
echo "You can use Postman!"
