sudo apt-get install cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev python3 cargo -y &&

git clone https://github.com/alacritty/alacritty.git &&

cd alacritty &&

cargo build --release &&

sudo cp target/release/alacritty /usr/local/bin &&

sudo cp extra/logo/alacritty-term.svg /usr/share/pixmaps/Alacritty.svg &&

sudo desktop-file-install extra/linux/Alacritty.desktop &&

sudo update-desktop-database &&

sudo mkdir -p /usr/local/share/man/man1 &&

gzip -c extra/alacritty.man | sudo tee /usr/local/share/man/man1/alacritty.1.gz > /dev/null &&

echo "source $(pwd)/extra/completions/alacritty.bash" >> ~/.bashrc

