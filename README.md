# Apps List

This is the apps that I use on my ubuntu os and if you want to use it too just run index.sh file and it will install all the apps for you ,
below is the list of apps which will be installed.

# Ubuntu Core Apps

    - Thunar (file manager)
    (and it will remove the default file manager nautilus)
    - curl ( https://developer.ibm.com/articles/what-is-curl-command/ )
    - gnome-tweaks (for tweaking your os)
    - virtualbox (app to run different os virtually)
    - mpv ( the best and open source app for consuming media )
    - htop ( system monitoring tool in shell )

# Browser Apps

    - it will install brave browser

# Programming Apps

    - As a code editor it will install VsCode
    - for making api request it will install postman

# Music Apps

    - It will install spotify as your music app

# NodeJS

    - First it will install nvm which is node version manager so that one can use different different node version on the system
    - then it will install current lts version of node and npm
    - then it will install these packages as global package
        - sol
        - pm2
        - @vue-cli
        - nodemon
        - localtunnel
        - serverless
        - yarn
        - serve

# Docker

    - It will install docker and docker-compose
      - then it will pull these 3 images from docker hub
      - mongodb
      - mysql
      - postgres

# Utility apps

    - it will install anydesk.
