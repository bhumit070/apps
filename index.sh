postFix="installed successfully"
isValidYes='y'

# function to send notification to user
notify_send() {
	notify-send --urgency=low "$1"
}

# function to set git credentials
setup_git_credentials() {
	echo "Please Enter you git username"
	read gitUsername

	echo "Please Enter your email to display in git"
	read gitEmail

	echo "git config --global user.name $gitUsername" >> git.sh
	echo "git config --global user.email $gitEmail" >> git.sh
	chmod +x git.sh
}

# function to install git
install_git() {
	echo "git not found so installing it..."
	sudo apt-get install git -y
}

# ask for git username and email store it in sh file and make it executable
echo "DO you want to setup global git Config? \npress y for Yes"
read isGlobalGitConfig

if [ "$isGlobalGitConfig" = "$isValidYes" ]
then
	git || install_git
	setup_git_credentials && ./git.sh
fi

echo "Do you want to reboot your macine after script completes? \npress y for yes"
read isReboot

# Update and Upgrade Packages
sudo apt-get update -y && sudo apt-get upgrade -y

# some must have apps on ubuntu
sudo apt-get install curl thunar gnome-tweaks virtualbox mpv htop -y
sudo apt-get remove nautilus

# browser 
./brave-browser.sh && notify_send "Brave Browser $postFix"

# programming apps
./vscode.sh && notify_send "Vscode $postFix"
./postman.sh && notify_send "Postman $postFix"

# Music app
./spotify.sh && "Spotify $postFix"

# install node and some npm packages 
./nvm.sh &&
./npm.sh

# Install docker
./docker.sh

# anydesk 
sudo ./anydesk.sh


if [ "$isReboot" = "$isValidYes" ]
then
	notify_send "Rebooting in 5 seconds"
	sleep 5
	reboot || systemctl -i reboot
else
	notify_send "All apps are installed Successfully Enjoy!, Thanks for using the script"
fi

